# Titanium Launcher

A simple and small version control launcher for Cosmic Reach

## Features
- One-click game updates
- Small footprint
- Written in Rust
- Cool themes idk

## Installation

#TODO: Releases

### Build From Source

Have `git` and `Cargo` installed to your system and on your PATH (Recommended: use [Rustup](https://github.com/rust-lang/rustup))

```sh
git clone "https://codeberg.org/Onezoop/Titanium";
cd Titanium;
mkdir versions versions-zips
```

go to "https://itch.io/user/settings/api-keys", log in, then copy and paste your key in a new file called ".TOKEN.txt"

```sh
cargo run
```



License: GPLv3
