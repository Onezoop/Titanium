#![allow(unused_imports, unused_variables, unused_attributes, dead_code)]
use iced::executor;
use iced::keyboard;
use iced::mouse;
use iced::subscription;
use iced::theme;
use iced::widget::Row;
use iced::widget::{
    button, canvas, checkbox, column, container, horizontal_space, pick_list, progress_bar, row,
    scrollable, text, Column,
};
use iced::{
    color, Alignment, Application, Command, Element, Font, Length, Point, Rectangle, Renderer,
    Settings, Subscription, Theme,
};
use std::future::Future;

#[tokio::main]
async fn main() -> iced::Result {
    LauncherLayout::run(Settings::default())
}

#[derive(Debug)]
struct Download {
    id: usize,
    state: State,
}

#[derive(Debug)]
enum State {
    Idle,
    Downloading { progress: f32 },
    Finished,
    Errored,
}

#[derive(Debug)]
struct LauncherLayout {
    page: LauncherPage,
    game_version_selected: Option<String>,
    theme: Theme,
    download_state: DownloadState,
    downloads: Vec<Download>,
    game_instances: Vec<run_process::GameInstance>,
    last_id: usize,
}

mod logic;
mod run_process;

#[derive(Debug, Clone)]
enum DownloadState {
    Idle,
    Downloading,
    Extracting,
}

#[derive(Debug, Clone)]
enum Message {
    ThemeSelected(Theme),
    StartGame(Option<String>),
    ChangeGameVersionSelcted(String),
    Add,
    Download(usize),
    DownloadProgressed((usize, logic::Progress)),
    CheckForUpdates,
}

impl LauncherLayout {
    fn is_version_selected(&self) -> bool {
        return self.game_version_selected != None;
    }
}

impl Application for LauncherLayout {
    type Message = Message;
    type Theme = Theme;
    type Executor = executor::Default;
    type Flags = ();

    fn new(_flags: Self::Flags) -> (Self, Command<Message>) {
        (
            Self {
                page: LauncherPage::default(),
                theme: Theme::Dracula,
                game_version_selected: None,
                download_state: DownloadState::Idle,
                downloads: vec![],
                game_instances: vec![],
                last_id: 0,
            },
            Command::none(),
        )
    }

    fn title(&self) -> String {
        format!("Launcher")
    }

    fn update(&mut self, message: Self::Message) -> Command<Message> {
        match message {
            Message::ThemeSelected(theme) => {
                self.theme = theme;
            }
            Message::StartGame(version) => {
                run_process::run_game(version.unwrap());
            }
            Message::ChangeGameVersionSelcted(version) => {
                self.game_version_selected = Some(version)
            }
            Message::Add => {
                self.last_id += 1;
                self.downloads.push(Download::new(self.last_id));
                if let Some(download) = self.downloads.get_mut(self.last_id - 1) {
                    download.start();
                }
            }
            Message::Download(index) => {
                if let Some(download) = self.downloads.get_mut(index) {
                    download.start();
                }
            }
            Message::DownloadProgressed((id, progress)) => {
                if let Some(download) = self.downloads.iter_mut().find(|download| download.id == id)
                {
                    download.progress(progress);
                }
            }
            Message::CheckForUpdates => {}
        }

        Command::none()
    }

    fn subscription(&self) -> Subscription<Message> {
        Subscription::batch(self.downloads.iter().map(Download::subscription))
    }
    /*fn subscription(&self) -> Subscription<Message> {
        use keyboard::key;


        keyboard::on_key_release(|key, _modifiers| match key {
            keyboard::Key::Named(key::Named::ArrowLeft) => Some(Message::Previous),
            keyboard::Key::Named(key::Named::ArrowRight) => Some(Message::Next),
            _ => None,
        })
    }*/

    fn view(&self) -> Element<Message> {
        let header = row![
            text(self.page.title).size(20).font(Font::MONOSPACE),
            horizontal_space(),
            //checkbox("Explain", self.explain).on_toggle(Message::ExplainToggled),
            pick_list(Theme::ALL, Some(&self.theme), Message::ThemeSelected),
        ]
        .spacing(20)
        .align_items(Alignment::Center);

        let version_selector_and_play = row![
            button("Play Game")
                .padding([10, 10])
                .on_press(Message::StartGame(self.game_version_selected.clone())),
            horizontal_space(),
            pick_list(
                logic::get_downloaded_versions(),
                self.game_version_selected.clone(),
                Message::ChangeGameVersionSelcted
            ),
        ]
        .spacing(20)
        .align_items(Alignment::Center);

        let update_and_upzip_buttons = row![
            button("Update Versions Downloaded!")
                .padding([10, 10])
                .on_press(Message::Add),
            horizontal_space(),
        ]
        .spacing(20)
        .align_items(Alignment::Center);

        let downloads = Column::with_children(self.downloads.iter().map(Download::view))
            .spacing(20)
            .align_items(Alignment::End);
        /*
                container(downloads)
                    .width(Length::Fill)
                    .height(Length::Fill)
                    .center_x()
                    .center_y()
                    .padding(20)
                    .into();
        */
        column![
            header,
            version_selector_and_play,
            update_and_upzip_buttons,
            downloads
        ]
        .spacing(10)
        .padding(20)
        .into()
    }

    fn theme(&self) -> Theme {
        self.theme.clone()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct LauncherPage {
    title: &'static str,
    view: fn() -> Element<'static, Message>,
}

impl LauncherPage {
    fn view(&self) -> Element<Message> {
        (self.view)()
    }
}

impl Default for LauncherPage {
    fn default() -> Self {
        Self {
            title: "Launcher",
            view: centered,
        }
    }
}

fn centered<'a>() -> Element<'a, Message> {
    container(text("I am centered!").size(50))
        .width(Length::Fill)
        .height(Length::Fill)
        .center_x()
        .center_y()
        .into()
}

impl Download {
    pub fn new(id: usize) -> Self {
        Download {
            id,
            state: State::Idle,
        }
    }

    pub fn start(&mut self) {
        match self.state {
            State::Idle { .. } | State::Finished { .. } | State::Errored { .. } => {
                self.state = State::Downloading { progress: 0.0 };
            }
            State::Downloading { .. } => {}
        }
    }

    pub fn progress(&mut self, new_progress: logic::Progress) {
        if let State::Downloading { progress } = &mut self.state {
            match new_progress {
                logic::Progress::Started => {
                    *progress = 0.0;
                }
                logic::Progress::Advanced(percentage) => {
                    *progress = percentage;
                }
                logic::Progress::Finished => {
                    self.state = State::Finished;
                }
                logic::Progress::Errored => {
                    self.state = State::Errored;
                }
            }
        }
    }

    pub fn subscription(&self) -> Subscription<Message> {
        match self.state {
            State::Downloading { .. } => {
                logic::file_function(self.id).map(Message::DownloadProgressed)
            }
            State::Idle => Subscription::none(),
            State::Finished => Subscription::none(),
            State::Errored => Subscription::none(), //_ => Subscription::none(),
        }
    }

    pub fn view(&self) -> Element<Message> {
        let current_progress = match &self.state {
            State::Idle { .. } => 0.0,
            State::Downloading { progress } => *progress,
            State::Finished { .. } => 100.0,
            State::Errored { .. } => 0.0,
        };

        let progress_bar = progress_bar(0.0..=100.0, current_progress);

        let control: Element<_> = match &self.state {
            State::Idle => button("Start the download!")
                .on_press(Message::Download(self.id))
                .into(),
            State::Finished => column!["Download finished!", button("Start again")]
                .spacing(10)
                .align_items(Alignment::Center)
                .into(),
            State::Downloading { .. } => {
                text(format!("Downloading... {current_progress:.2}%")).into()
            }
            State::Errored => column![
                "Something went wrong :(",
                button("Try again").on_press(Message::Download(self.id)),
            ]
            .spacing(10)
            .align_items(Alignment::Center)
            .into(),
        };

        Column::new()
            .spacing(10)
            .padding(10)
            .align_items(Alignment::Center)
            .push(progress_bar)
            //.push(control)
            .into()
    }
}
