use std::io::Read;
use std::path::{Path, PathBuf};
use std::process::Command;

#[derive(Debug)]
pub struct GameInstance;

pub fn run_game(version: String) {
    let mut game_jar_path = PathBuf::new();
    let game_version_name: String = version;
    let game_dir_name = format!("{game_version_name}");
    let mut game_jar_name = game_version_name.replace("_jar", "");
    game_jar_name = format!("Cosmic Reach-{game_jar_name}.jar");
    game_jar_path.push("versions");
    game_jar_path.push(game_dir_name);
    game_jar_path.push(game_jar_name);

    let mut game_process = Command::new("java")
        .arg("-jar")
        .arg(game_jar_path.into_os_string().to_str().unwrap())
        .stdout(std::process::Stdio::piped())
        .spawn()
        .unwrap();

    println!("started sessions");
    let mut fake_stdout = game_process.stdout.take().unwrap();
    let mut output_string: String = "".to_string();
    fake_stdout.read_to_string(&mut output_string);
    println!("{}", output_string);
}
