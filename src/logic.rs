#![allow(dead_code, unused_imports)]
use iced::futures::stream;
use iced::Subscription;
use reqwest::Version;
use serde::{Deserialize, Serialize};
use serde_json::json;
use serde_json::Map;
use serde_json::Serializer;
use std::any::type_name;
use std::error::Error;
use std::fs::{self, create_dir, read, read_dir, File};
use std::io::{prelude::*, Read};
use std::ops::Deref;
use std::path::{Path, PathBuf};
use std::{collections::HashMap, env::current_dir};
use tokio::io::AsyncWriteExt;
use zip_extract::*;

#[derive(Serialize, Deserialize)]
struct Builds {
    uploads: Vec<Build>,
}

#[derive(Serialize, Deserialize)]
struct Build {
    created_at: String,
    channel_name: String,
    build: BuildInfo,
    id: i32,
}

#[derive(Serialize, Deserialize)]
struct BuildInfo {
    version: i32,
    user_version: String,
    id: i32,
}

#[derive(Serialize, Deserialize)]
struct DownloadURL {
    url: String,
}

async fn test() {
    //let token_file_path: PathBuf = [".TOKEN.txt"].iter().collect();

    //let version = "0.1.14";
    //run_game(version.to_string());

    //let current_uploads = get_current_uploads(&token).await.unwrap();
    //write_uploads(current_uploads)

    /*for build in game_builds_uploaded.uploads {
        if build.channel_name == "jar" {
            download_game_build(&token, &build, &build.build.user_version).await;
            extract_game_zip(&build.build.user_version);
        }
    }*/
}

fn read_token(token_file_path: PathBuf) -> String {
    return fs::read_to_string(token_file_path).expect("whoops");
}

pub fn write_toke(token: String) {
    let token_file = File::create(".TOKEN.txt");
    let _ = token_file.unwrap().write(token.as_bytes());
}

async fn download_game_build(game_build: &Build, version: &String) {
    let mut token_file_path = PathBuf::new();
    token_file_path.push(".TOKEN.txt");
    let token = read_token(token_file_path);
    let game_build_id = game_build.id;
    let game_build_version = version;
    let game_request_download_url =
        format!("https://itch.io/api/1/{token}/upload/{game_build_id}/download");
    let resp = reqwest::get(game_request_download_url).await;
    let download_link_json = resp.unwrap().json::<DownloadURL>().await.unwrap();
    let download_link = download_link_json.url;
    let download_request = reqwest::get(download_link).await;
    let content = download_request.unwrap().bytes().await.unwrap();
    let mut game_zip =
        File::create(format!("./versions-zips/{game_build_version}_jar.zip")).unwrap();
    let _ = game_zip.write_all(&content);
    extract_game_zip(game_build_version);
}

async fn download_current_game_build_link(game_build: &Build) {
    let mut token_file_path = PathBuf::new();
    token_file_path.push(".TOKEN.txt");
    let token = read_token(token_file_path);
    let current_uploads = get_current_uploads(token.clone()).await.unwrap();
    for build in current_uploads.uploads {
        if build.channel_name == "jar" {
            let game_build_id = build.id;
        }
    }
    let game_build_id = game_build.id;
    let game_request_download_url =
        format!("https://itch.io/api/1/{token}/upload/{game_build_id}/download");
    let resp = reqwest::get(game_request_download_url).await;
    let download_link_json = resp.unwrap().json::<DownloadURL>().await.unwrap();
    let download_link = download_link_json.url;
}

pub fn get_downloaded_versions() -> Vec<String> {
    let mut version_downloaded_path: PathBuf = PathBuf::new();
    version_downloaded_path.push("versions");
    let game_versions = read_dir(version_downloaded_path).unwrap();
    let mut out_vec: Vec<String> = Vec::new();
    for game_version in game_versions {
        out_vec.push(
            game_version
                .unwrap()
                .path()
                .file_name()
                .unwrap()
                .to_str()
                .unwrap()
                .to_string(),
        );
    }
    return out_vec;
}

pub fn get_downloaded_zip_versions() -> Vec<String> {
    let mut version_downloaded_path: PathBuf = PathBuf::new();
    version_downloaded_path.push("versions-zips");
    let game_versions = read_dir(version_downloaded_path).unwrap();
    let mut out_vec: Vec<String> = Vec::new();
    for game_version in game_versions {
        out_vec.push(
            game_version
                .unwrap()
                .path()
                .file_name()
                .unwrap()
                .to_str()
                .unwrap()
                .to_string(),
        );
    }
    return out_vec;
}

fn extract_game_zip(version: &String) {
    let version_name = format!("{version}_jar");
    let target_dir: PathBuf = ["versions", &version_name].iter().collect();
    let mut archive_file_path: PathBuf = ["versions-zips"].iter().collect();
    archive_file_path.push(format!("{version_name}.zip"));
    //println!("{}", archive_file_path.into_os_string().to_str().unwrap());
    let mut archive = File::open(archive_file_path).unwrap();
    let mut buffer = Vec::new();
    let _ = archive.read_to_end(&mut buffer);
    //println!("{}", archive_file_path.into_os_string().to_str().unwrap());
    let _ = extract(std::io::Cursor::new(buffer), &target_dir, false);
}

async fn get_me_page(token: &String) -> Result<String, Box<dyn Error>> {
    let about_me_page_api_url = format!("https://itch.io/api/1/{token}/me");
    let resp = reqwest::get(about_me_page_api_url).await?.json().await?;
    return Ok(resp);
}

pub async fn get_download_link_current_upload() -> Option<String> {
    let mut token_file_path = PathBuf::new();
    token_file_path.push(".TOKEN.txt");
    let token = read_token(token_file_path);
    let current_uploads = get_current_uploads(token.clone()).await.unwrap();
    for build in current_uploads.uploads {
        if build.channel_name == "jar" {
            let current_jar_build_id = build.id;
            let game_request_download_url =
                format!("https://itch.io/api/1/{token}/upload/{current_jar_build_id}/download");
            let resp = reqwest::get(game_request_download_url).await;
            let download_link_json = resp.unwrap().json::<DownloadURL>().await.unwrap();
            let download_link = download_link_json.url;
            return Some(download_link);
        }
    }
    return None;
}

async fn get_current_uploads(token: String) -> Result<Builds, Box<dyn Error>> {
    let cosmic_reach_uploads_api_url =
        format!("https://itch.io/api/1/{token}/game/2557309/uploads");
    let resp = reqwest::get(cosmic_reach_uploads_api_url)
        .await
        .unwrap()
        .text()
        .await
        .unwrap();
    let return_builds = serde_json::from_str(&resp).unwrap();
    Ok(return_builds)
}

pub async fn get_current_uploaded_version() -> Option<String> {
    let mut token_file_path = PathBuf::new();
    token_file_path.push(".TOKEN.txt");
    let token = read_token(token_file_path);
    let current_uploads = get_current_uploads(token).await.unwrap();
    for build in current_uploads.uploads {
        if build.channel_name == "jar" {
            return Some(build.build.user_version);
        }
    }
    return None;
}

fn write_uploads(upload: Builds) {
    let today = chrono::Local::now().date_naive();
    let mut today_upload_file_path: PathBuf = PathBuf::new();
    today_upload_file_path.push("uploads");
    today_upload_file_path.push(format!("{today}.json"));
    let mut today_upload = File::create(today_upload_file_path).unwrap();
    let _ = today_upload.write_all(serde_json::to_string(&upload).unwrap().as_bytes());
}

use iced::subscription;

use std::hash::Hash;

// Just a little utility function
pub fn file_function<I: 'static + Hash + Copy + Send + Sync>(
    id: I,
) -> iced::Subscription<(I, Progress)> {
    subscription::unfold(id, State::Ready, move |state| //download(id, state))
    {
        download(id, state, "1.0")
    })
}

#[derive(Debug, Hash, Clone)]
pub struct Download<I> {
    id: I,
}

async fn download<I: Copy>(id: I, state: State, random_string: &str) -> ((I, Progress), State) {
    let mut write_buffer: Vec<u8> = Vec::new();
    let ret_val: ((I, Progress), State) = match state {
        State::Ready => {
            //let url = "https://www.wiki.gg"; //test url
            let url = get_download_link_current_upload().await.unwrap(); //real url
            let version = get_current_uploaded_version().await.unwrap();
            let response = Some(reqwest::get(url).await);

            match response.unwrap() {
                Ok(response) => {
                    if let Some(total) = response.content_length() {
                        (
                            (id, Progress::Started),
                            State::Downloading {
                                response,
                                total,
                                downloaded: 0,
                                version,
                            },
                        )
                    } else {
                        ((id, Progress::Errored), State::Finished)
                    }
                }
                Err(_) => ((id, Progress::Errored), State::Finished),
            }
        }
        State::Downloading {
            mut response,
            total,
            downloaded,
            version,
        } => {
            match response.chunk().await {
                Ok(Some(chunk)) => {
                    //let _ = file_test.write_all(write_buffer.as_slice()).await.unwrap();
                    let downloaded = downloaded + chunk.len() as u64;

                    write_buffer.extend(chunk.iter().clone());
                    let mut file_test = std::fs::OpenOptions::new()
                        .create(true)
                        .append(true)
                        .open(format!("versions-zips/{version}_jar.zip"))
                        .unwrap();
                    let _ = file_test.write_all(write_buffer.as_slice()).unwrap();
                    let percentage = (downloaded as f32 / total as f32) * 100.0;
                    (
                        (id, Progress::Advanced(percentage)),
                        State::Downloading {
                            response,
                            total,
                            downloaded,
                            version,
                        },
                    )
                }
                Ok(None) => {
                    extract_game_zip(&version);
                    ((id, Progress::Finished), State::Finished)
                }
                Err(_) => ((id, Progress::Errored), State::Finished),
            }
        }
        State::Finished => {
            // We do not let the stream die, as it would start a
            // new download repeatedly if the user is not careful
            // in case of errors.
            iced::futures::future::pending().await
        }
    };
    return ret_val;
}

#[derive(Debug, Clone)]
pub enum Progress {
    Started,
    Advanced(f32),
    Finished,
    Errored,
}

pub enum State {
    Ready,
    Downloading {
        response: reqwest::Response,
        total: u64,
        downloaded: u64,
        version: String,
    },
    Finished,
}
